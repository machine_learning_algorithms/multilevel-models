# mixed effects model 

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import sklearn as sk
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from scipy import stats
import statsmodels.api as sm
import statsmodels.formula.api as smf
plt.ion()

# read in data
data = pd.read_csv('../data/website_bounce_rates.csv',sep='\t')

# scale variables; zero mean, unit var
data['age_scaled'] = preprocessing.scale(data.age.values)

# histogram distribution of features 
plt.figure(); plt.hist(data.bounce_time)
plt.figure(); plt.hist(data.age)
plt.figure(); plt.hist(data.age_scaled)

# linear regression
model = LinearRegression(fit_intercept=True)
x = data.age_scaled
y = data.bounce_time
model.fit(x[:,np.newaxis],y) # y-int
xfit = np.linspace(-3,3,1000)
yfit = model.predict(xfit[:,np.newaxis])
plt.figure; plt.scatter(x,y)
plt.plot(xfit,yfit)

# model params and rmse
model.coef_[0]
model.intercept_
y_predict = model.predict(x.values.reshape(-1,1))
rmse = np.sqrt(((y-y_predict)**2).values.mean())

# residuals, lowess
resid = y - y_predict
plt.figure()
ax = sns.residplot(x='age_scaled',y='bounce_time',data=data,lowess=True)
ax.set(ylabel='Observed - Prediction')

# categorical plots
sns.catplot(x='county',y='bounce_time',data=data,kind='swarm')

# linear regression by county, categorial plt
grid = sns.lmplot(x='age_scaled',y='bounce_time',col='county',sharex=False,col_wrap=4,data=data,height=4)
sns.catplot(x="location", y="bounce_time", col="county", col_wrap=4, sharey=False, data=data, kind = "swarm")

# model county as fixed effect
# time[c_i] = b0 + b1*age + c_i
counties = data.county.unique()
data_new = pd.concat([data,pd.get_dummies(data.county)],axis=1) # 1-hot
model = LinearRegression(fit_intercept=True)
x = data_new.loc[:,np.concatenate((['age_scaled'],counties))]
y = data.bounce_time
model.fit(x,y)
performance = pd.DataFrame()
performance['predicted']= model.predict(x)
performance['age_scaled'] = data.age_scaled
performance['residuals'] = model.predict(x) - data.bounce_time
plt.figure()
ax = sns.residplot(x = 'age_scaled',y='residuals',data=performance,lowess=True)
ax.set(ylabel='Observed - Prediction')

# mixed effects models
md = smf.mixedlm('bounce_time ~ age_scaled',data,groups=data['county'])
mdf = md.fit()
print(mdf.summary())
performance = pd.DataFrame()
performance["residuals"] = mdf.resid.values
performance["age_scaled"] = data.age_scaled
performance["predicted"] = mdf.fittedvalues
plt.figure()
sns.lmplot(x = "predicted", y = "residuals", data = performance)
plt.figure()
ax = sns.residplot(x = "age_scaled", y = "residuals", data = performance, lowess=True)
ax.set(ylabel='Observed - Prediction')
# rmse
y_predict = mdf.fittedvalues
rmse = np.sqrt(((y-y_predict)**2).values.mean())

# now add random intercept and slope wrt age
md = smf.mixedlm('bounce_time~age_scaled',data,groups=data['county'],re_formula='~age_scaled')
mdf = md.fit()
print(mdf.summary())
performance = pd.DataFrame()
performance["residuals"] = mdf.resid.values
performance["age_scaled"] = data.age_scaled
performance["predicted"] = mdf.fittedvalues
plt.figure()
sns.lmplot(x = "predicted", y = "residuals", data = performance)
plt.figure()
ax = sns.residplot(x = "age_scaled", y = "residuals", data = performance, lowess=True)
ax.set(ylabel='Observed - Prediction')
y_predict = mdf.fittedvalues
rmse = np.sqrt(((y-y_predict)**2).values.mean())

# calculate significance of liear regression
lm = LinearRegression(fit_intercept=True)
x = data.age
y = data.bounce_time
lm.fit(x[:,np.newaxis],y)
params = np.append(lm.intercept_,lm.coef_)
predictions = lm.predict(x.values.reshape(-1, 1))
newx = pd.DataFrame({"Constant":np.ones(len(x))}).join(pd.DataFrame(x))
mse = (sum((y-predictions)**2))/(len(newx)-len(newx.columns))
var_b = mse*(np.linalg.inv(np.dot(newx.T,newx)).diagonal())
sd_b = np.sqrt(var_b)
ts_b = params / sd_b
p_values = [2*(1-stats.t.cdf(np.abs(i),(len(newx)-1))) for i in ts_b]
names = ["intercept", "age"]
summary = pd.DataFrame()
summary["names"],summary["Coefficients"],summary["Standard Errors"] = [names,params,sd_b]
summary["t values"],summary["Probabilites"] = [ts_b,p_values]
print(summary)

# nested mixed-effects model 
data['location_county'] = data['location'] + '_' + data['county']
md = smf.mixedlm("bounce_time ~ age_scaled", data, groups=data["location_county"], re_formula="~age_scaled")
mdf = md.fit()
print(mdf.summary())
y_predict = mdf.fittedvalues
mse = np.sqrt(((y-y_predict)**2).mean())

